#!/usr/bin/env python3

import py8chan, time, shutil, requests, random, os, json, itertools, argparse
from multiprocessing.dummy import Pool as ThreadPool

# ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥

parser = argparse.ArgumentParser()
parser.add_argument("-c", "--cache", help="cache size", type=float, required=True)
parser.add_argument("-j", "--json", help="json path", type=str, required=True)
parser.add_argument("-m", "--memes", help="memes path", type=str, required=True)
args = parser.parse_args()

# ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥

meme_folder = args.memes                # Download folder
meme_data = {}							# Dictionary with all the memes info
memes_json = args.json+'/memes.json'    # The same as json
meme_data_index = 0						# To keep track of the memes keys
memes = []								# A cache with all the memes seen
pol = py8chan.Board('pol')				# /pol/
leftypol = py8chan.Board('leftypol')	# /leftypol/

# ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥

class colors:
	blue = '\033[94m\033[40m'
	yellow = '\033[93m\033[40m'
	green = '\033[92m\033[40m'
	end = '\033[0m'

# ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥

def load_json():
	"""
	If found, loads already existing meme database,
	and adjusts the current track of memes keys.
	"""
	global meme_data
	global meme_data_index
	if os.path.isfile(memes_json):
		print(colors.yellow+'found memes.json'+colors.end)
		with open(memes_json, 'r') as inputfile:
			meme_data = json.load(inputfile)
		meme_data_index = int(max(meme_data.keys(), key=int)) + 1
		print(meme_data_index)

	
def trim_json(size):
	"""
	Removes old keys until the right size is reached.
	"""
	global meme_data
	if len(meme_data) > size:
		oldest_key = min(meme_data.keys(), key=int)
		del meme_data[oldest_key]
		print('removed key '+str(oldest_key))
		trim_json(size)


def write_json():
	"""
	Dumps the meme_data dictionary into a json file and,
	before writing, trims the dictionary if too big.
	"""
	global meme_data
	trim_json(100)
	#if len(meme_data) > 200:
	#	iter_meme_data = iter(meme_data.items())
	#	meme_data = dict(itertools.islice(iter_meme_data, 50, None))
	#	print('trimming!')
	with open(memes_json, 'w') as outfile:
		json.dump(meme_data, outfile)


def folder_size(path=meme_folder):
	"""
	Returns folder size.
	"""
	total = 0
	for entry in os.scandir(path):
		if entry.is_file():
			total += entry.stat().st_size
		elif entry.is_dir():
			total += folder_size(entry.path)
	return total


def clean_meme_cache():
	"""
	Removes old memes until max folder size is reached.
	"""
	if folder_size() >=  args.cache*1000000000:
		oldest_file = min(os.listdir(meme_folder), key=lambda f: os.path.getctime("{}/{}".format(meme_folder, f)))
		print('delete : '+oldest_file)
		os.remove(meme_folder+'/'+oldest_file)
		clean_meme_cache()


def random_sleep(max_duration):
	"""
	Sleeps for 0-max_duration seconds.
	"""
	random_duration = random.random()*max_duration
	time.sleep(random_duration)


def save_meme(url, cached_memes, board, topic):
	"""
	If meme file extension is not blacklisted,
	saves the meme to file and add new entry in the meme_data dictionary,
	including board name, thread topic, and filename.
	"""
	global meme_data
	global meme_data_index

	filename = url.rsplit('/', 1)[-1]
	file_extension = filename.split(".")[-1]

	if filename != 'deleted' and file_extension not in ['webm','mp4','pdf'] and filename not in cached_memes:
		print(colors.green+'incoming'+colors.end+' '+filename)
		response = requests.get(url, stream=True)
		with open(meme_folder+'/'+filename, 'wb') as out_file:
			shutil.copyfileobj(response.raw, out_file)
		meme_data[meme_data_index] = [board, topic, filename]
		print(meme_data_index)
		meme_data_index += 1
		del response

	else:
		print(colors.blue+'ignored!'+colors.end+' '+filename)


def get_fresh_memes(thread, board, topic):
	"""
	Compares the list of memes from thread with list of cached memes.
	New memes are saved and their name added to the cache.
	""" 
	global memes
	thread_memes = []
	cached_memes = os.listdir(meme_folder)

	for file in thread.file_objects():
		thread_memes.append((file.file_url))

	new_memes = list(set(thread_memes) - set(memes))

	for file in new_memes:
		save_meme(file, cached_memes, board, topic)	

	memes += new_memes
		

def get_fresh_posts(thread):
	"""
	Retrieves a thread's new posts and memes.
	Also returns board and topic for convenience,
	because I made a mess with scopes.
	"""
	board = 'nope'
	topic = 'nope'
	if not thread.closed:
		try:
			new_posts = str(thread.update())	# !!! See BRAINDUMP
			topic = str(thread.topic.subject)	# potential UTF8 problem!
			if topic == 'None' : topic = ''
			url = thread.url
			sticky = '(sticky)' if thread.sticky == True else ''
			board = thread.url.split('/')[3]

			print(url+' ('+new_posts+') - '+topic+' '+sticky)
			#print(url+' ('+new_posts+') - '+sticky)

		except requests.exceptions.RequestException as error:
			print(error)

	return (board, topic)


def process_thread_cache(thread):
	"""
	Retrieves all the posts and memes from given thread.
	Adds all the memes to the cache,
	Nothing is downloaded, only used in the init phase of the program.
	"""
	global memes
	get_fresh_posts(thread)
	thread_memes = []
	cached_memes = os.listdir(meme_folder)
	for file in thread.file_objects():
		thread_memes.append((file.file_url))
	new_memes = list(set(thread_memes) - set(memes))
	memes += new_memes


def process_thread(thread):
	"""
	Updates a thread, download the new memes and update the json file.
	Also sleeps a bit to not hammer server.
	"""
	(board, topic) = get_fresh_posts(thread)
	if board is not "nope":
		get_fresh_memes(thread, board, topic)
		write_json()


def start_meme_cache():
	"""
	Multi-threaded download of all current threads info and attached memes,
	to populate the meme cache.
	"""
	pool = ThreadPool(4)
	threads = pol.get_all_threads()+leftypol.get_all_threads()
	pool.map(process_thread_cache, threads)
	pool.close()
	pool.join()		
	

# ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥ ♥

load_json()

print(colors.yellow+'populating meme cache'+colors.end)
start_meme_cache()
print(colors.yellow+'caching complete!'+colors.end)


while True:
	threads = pol.get_threads()+leftypol.get_threads()
	random.shuffle(threads)
	[process_thread(thread) for thread in threads]

	print(colors.yellow+'New round!'+colors.end)

	clean_meme_cache()
