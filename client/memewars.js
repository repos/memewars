// tfw...

(function() {

	var memes_json = 'memes.json';
	var memes_path = 'img';

	var memes;
	var meme_index;
	var first_run = 1;

	function is_int(value) {
		var x;
		return isNaN(value) ? !1 : (x = parseFloat(value), (0 | x) === x);
	}

	function debug_counter(json) {
		var size = Object.keys(json).length;
		console.log('size: '+size);
		}

	function get_highest_key(json) {
		var keys_as_string = Object.keys(json);
		var keys_as_int = keys_as_string.map(function (x) { 
    		return parseInt(x); 
		});
		var highest_key = Math.max.apply(null, keys_as_int);
		
		if (is_int(highest_key)) {
			return highest_key;
		} else {
			return 0;
		}
	}

	function debug_all(json) {
		debug_counter(json);
		var highest_key = get_highest_key(json);
		console.log('highest key: '+highest_key);
	}
	
	function get_memes() {
		var request = new XMLHttpRequest();
		request.open('GET', memes_json + '?' + (new Date()).getTime(), true);

		request.onload = function() {
			if (request.status >= 200 && request.status < 400) {
				memes = JSON.parse(request.responseText);
				if (first_run == 1) {
					meme_index = get_highest_key(memes);
					debug_counter(memes);
					console.log('highest key is '+meme_index);
					first_run = 0;
				}
			} else {
				console.log('meh...');
			}
		}
		
		request.send();		
	}

	function next_meme() {
		debug_all(memes);
		console.log('trying '+meme_index.toString());
		meme = memes[meme_index.toString()];
		if (typeof meme != 'undefined') {
			console.log(meme_index+' '+meme);
			var board = meme[0];
			var topic = meme[1];
			var file = meme[2];
			document.getElementById(board).src = memes_path+'/'+file;
			document.getElementById(board+'topic').innerHTML = topic;
			
			meme_index += 1;
		}
	}

	function start_timer() {
		setInterval(get_memes, 3000);
		setInterval(next_meme, 2000);
	}

	start_timer();

})();


